import { Component, OnInit } from '@angular/core';
import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';

@Component({
  selector: 'app-challenge2',
  templateUrl: './challenge2.component.html',
  styleUrls: ['./challenge2.component.css']
})
export class Challenge2Component implements OnInit {

   numbers = new Array<any>()
   amount = 1;

  constructor() { }

  ngOnInit(): void {
  }

  calculate() {
    var inputNumbers = document.getElementsByTagName('input')
    for(var j = 0; j < 100; j++) {
      this.numbers.push(j + 1)
    }
    for(var i = 0; i < this.numbers.length; i++) {
      if( this.numbers[i] % inputNumbers.item(0).valueAsNumber == 0 && this.numbers[i] % inputNumbers.item(1).valueAsNumber == 0){
        this.numbers[i] = 'FB';
      }
      else if( this.numbers[i] % inputNumbers.item(0).valueAsNumber == 0){
        console.log("Number 1: " + inputNumbers.item(0).valueAsNumber)
        this.numbers[i] = 'F';
      }
      else if( this.numbers[i] % inputNumbers.item(1).valueAsNumber == 0){
        console.log("Number 2: " + inputNumbers.item(1).valueAsNumber)
        this.numbers[i] = 'B'
      }
    }
    this.amount++;
  }

}
