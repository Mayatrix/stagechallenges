var startTime = new Date().getTime();

function findValue(jsonObj, value, path) {
    if (jsonObj === null) {
        // do nothing
    } else if (typeof jsonObj == 'object') {
        for (key in jsonObj) {            
            findValue(jsonObj[key], value, path + key + '-> ');
        }
    } else if (jsonObj === value){
        console.log(path.slice(0,-3) + " = dailyprogrammer");
    }
}

var jsonObj1 = require("../../assets/6/challenges/challenge1.json");
var jsonObj2 = require("../../assets/6/challenges/challenge2.json");

findValue(jsonObj1, "dailyprogrammer", "");
findValue(jsonObj2, "dailyprogrammer", "");

var endTime = new Date().getTime();

console.log("It took " + (endTime - startTime) + " ms to find dailyprogrammer")