var fs = require('fs')

var input = fs.readFileSync('../../assets/1/input.txt', 'utf8');

var firstFilter = input.replace(/[^a-zA-Z \n]/g, " ");
var secondFilter = firstFilter.replace(/  +/g, ' ');
var result = secondFilter.replace(/[ ]/, '').trimLeft();

fs.writeFileSync('../../assets/1/output.txt', result);
