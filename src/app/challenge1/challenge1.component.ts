import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-challenge1',
  templateUrl: './challenge1.component.html',
  styleUrls: ['./challenge1.component.css']
})

export class Challenge1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  solve() {
    const input = document.getElementsByTagName('object')
    input.item(0).data= '../../assets/1/output.txt'
    console.log(input)
  }

}
